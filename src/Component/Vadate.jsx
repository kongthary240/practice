import React, { Component } from 'react'
import { Formik } from "formik";
import * as Yup from "yup";

export default class Vadate extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          obj: [],
        };
      }
      componentDidUpdate = () => {
       console.log(this.state.obj);
      };
      render() {
        return (
          <div>
            Validate
            <Formik
              initialValues={{ username: " ", password: "" }}
       
                  validationSchema={Yup.object({
                    username: Yup.string()
                      .max(8, <p >input less than 8 character</p>)
                      .min(2, <p style={{color: "green"}}>Please input 1 character</p>)
                      .matches(/^[a-z| A-Z]+$/, <p style={{color: "green"}}>Input text only</p> )
                      .required(<p style={{color: "green"}}>Required</p>),
                    password: Yup.string()
                      .required(<p style={{color: "green"}}>Required</p>)
                      .min(8, <p style={{color: "green"}}>Please input more than 8 character</p>)
                      .matches(/[a-zA-Z0-9]/)
                 


              })}
              //   validate={(values) => {
              //     if (!values.username) {
              //       errors.username = "Required";
              //     } else if (/^[a-z]/gi.test(values.username)) {
              //       errors.username = "Please input characte only";
              //     }
              //     if (!values.password) {
              //       errors.password = "Required";
              //     }
              //     return errors;
              //   }}
    
              onSubmit={(values, { setSubmitting }) => {
                setTimeout(() => {
                  this.setState({
                    obj: [...this.state.obj, values],
                  });
                  console.log(values);
                  setSubmitting(false);
                }, 200);
              }}
            >
              {({
                values,
                handleChange,
                errors,
                handleBlur,
                touched,
                handleSubmit,
                isSubmitting,

              }) => (
                <form onSubmit={handleSubmit}>
                  <label>Username:</label>
                  <input
                    type="text"
                    name="username"
                    value={values.username}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  ></input>
                  {errors.username && touched.username && errors.username}
    
                  <br />
                  <label>Password:</label>
                  <input
                    type="text"
                    name="password"
                    value={values.password}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  ></input>
                  {errors.username && touched.username && errors.username}
    
                  <br />
                  <button type="submit" disabled={isSubmitting}>
                    login
                  </button>
                </form>
              )}
            </Formik>
            {this.state.obj.map((item) => (
              <ol key={item}>
                <li>{item.username}</li>
                <li>{item.password}</li>
              </ol>
            ))}
          </div>
        );
      }
}
